#!/bin/bash

CS_FOLDER="/home/poka/dev/cesium-plus-pod"

PID=$(ps -aux | grep cesium-plus-pod-assembly | grep -v "grep" | head -n1 | awk '{ print $2 }')

if [[ $PID ]]; then
	kill $PID
	sleep 0.5
fi

cd $CS_FOLDER
mvn install -Prun -pl cesium-plus-pod-assembly >/dev/null 2>&1 &
