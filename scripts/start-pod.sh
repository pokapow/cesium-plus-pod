#!/bin/bash

CS_FOLDER="/home/poka/dev/cesium-plus-pod"

cd $CS_FOLDER
mvn install -Prun -pl cesium-plus-pod-assembly >/dev/null 2>&1 &
