#!/bin/bash

PID=$(ps -aux | grep cesium-plus-pod-assembly | grep -v "grep" | head -n1 | awk '{ print $2 }')

if [[ $PID ]]; then
	kill $PID
else
	echo "Cs+ is not loaded"
fi
